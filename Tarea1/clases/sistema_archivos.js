//lectura sincrona de archivoc

const fs = require('fs');

/*let datosRaw=fs.readFileSync('puntajes.json')
let puntajes = JSON.parse(datosRaw);

console.log(puntajes);

//escritura sincrona;

fs.writeFileSync('puntajes.json', JSON.stringify(puntajes));
*/

const getFile = fileName =>{
    return new Promise ((resolve, reject) =>{
        fs.readFile(fileName, (err,data)=>{
            if(err){
                reject(err);
                return;
            }
            resolve(data);
        })
    })
}

getFile('puntajes.json')
.then(JSON.parse)
.then(console.log)
.catch(console.error);
