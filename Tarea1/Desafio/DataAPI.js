//descargar archivos e la api
const fetch = require('node-fetch');
const status = response =>{
    if(response.status >= 200 && response.status<300){
        return Promise.resolve(response);
    }
    return Promise.reject(new Error(response.statusText));
}

const obtenerJson = response =>{return response.json();}


var promesa = fetch('https://mindicador.cl/api')
.then(status)
/*.then(r=> {return r.text()})
.then(JSON.parse)*/
.then(obtenerJson)


module.exports = promesa;